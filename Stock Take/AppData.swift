//
//  AppData.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 16/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import UIKit

// MARK: User Defaults
// To load data in Stock Take Info
enum CurrentStockTakeInfo: String {
    case currentBranch = "currentStockTakeBranch"
    case currentDate = "currentStockTakeDate"
    case currentLocation = "currentStockTakeLocation"
    case currentShelf = "currentStockTakeShelf"
    case currentUser = "currentStockTakeUser"
    case currentIsQtyOne = "currentStockTakeIsQtyOne"
}

// For Filter Selection in Report
enum StockTakeInfoList: String {
    case branchList = "stockTakeBranchList"
    case dateList = "StockTakeDateList"  // :(
    case locationList = "stockTakeLocationList"
    case shelfList = "stockTakeShelfList"
    case userList = "stockTakeUserList"
    case dateMin = "minStockTakeDate"
    case dateMax = "maxStockTakeDate"
    case recordNumber = "recordNumber"  // write: save stock take record; read: generate report
}

// For Export file format setup
enum ExportFileFormatSetup: String {
    case HeaderSetting = "HeaderSetting"
    case Delimiter = "Delimiter"
}

class AppData: NSObject {
    
}

public let SELECT_ALL = "All"

enum criteriaType {
    case branch, date, location, shelf, user
}

// MARK: Public Functions
public func activateButton(for button: UIButton, to status: Bool) {
    switch status {
    case true:
        button.isEnabled = true
        button.alpha = 1.0
    case false:
        button.isEnabled = false
        button.alpha = 0.5
    }
}

public func printDate(for date: Date, time: Bool) {
    // Set date
    let df = DateFormatter()
    //    df.dateFormat = "dd-MM-yyyy'T'HH:mm:ssZZZZZ"
    df.dateFormat = time ? "dd-MM-yyyy' 'HH:mm:ss" : "dd-MM-yyyy"
    print("\(df.string(from: date))")
}
