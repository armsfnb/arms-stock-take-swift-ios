//
//  SelectBatchNoViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 12/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//protocol SelectBatchNoViewControllerDelegate {
//    func didSelectBatchNo(_ sender: UIDatePicker, selectedBatchNo: String)
//}
//
//// MARK: Select Batch No View Controller
//class SelectBatchNoViewController: UITableViewController {
//
//    // MARK: Outlets
////    @IBOutlet var batchNoTableView: UITableView!
//
//    var delegate: StockTakeInfoViewController?
//    var selectedBatchNo: String?
//
//    lazy var batchNoList: [String]? = {
//        let batchNo = [String]()
//        return batchNo
//    }()
//
//
//    // MARK: Actions
//    @IBAction func barBtnCancel(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        self.loadBatchNo()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    // MARK: Core Date
//    func loadBatchNo() {
//        let fetchStockTakeRequests: NSFetchRequest<Stock_take> = Stock_take.fetchRequest()
//        fetchStockTakeRequests.propertiesToFetch = ["batch_no"]
//        fetchStockTakeRequests.returnsDistinctResults = true
//
//        do {
//            let stockTakes = try self.appContext().fetch(fetchStockTakeRequests)
//            if stockTakes.count > 0 {
//                for record in stockTakes {
//                    if self.batchNoList?.contains(record.batch_no!) == false {
//                        self.batchNoList?.append(record.batch_no!)
//                    }
//                }
//                self.batchNoList?.sort()
//            }
//        } catch let error {
//            print(error.localizedDescription)
//        }
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var count = 0
//        if let batchNoList = self.batchNoList {
//            count = batchNoList.count
//        }
////        print(count)
//        return count
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//        if let batchNo = self.batchNoList?[indexPath.row] {
//            cell.textLabel?.text = batchNo
//        }
//        return cell
//    }
//
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let batchNo = self.batchNoList?[indexPath.row] {
//            self.selectedBatchNo = batchNo
//            self.delegate?.selectedBatchNo = batchNo
//            self.delegate?.btnSelectBatchNo.setTitle(batchNo, for: .normal)
//            activateButton(for: (self.delegate?.btnGenerateReport)!, to: true)
//
//            self.dismiss(animated: true, completion: nil)
//        }
//    }
//
//}
//
