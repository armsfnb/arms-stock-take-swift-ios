//
//  SelectDateViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 28/03/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit

protocol SelectDateViewControllerDelegate {
    func didSelectDate(_ sender: UIDatePicker, selectedDate: Date)
}

// MARK: Select Date View Controller
class SelectDateViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var selectDatePicker: UIDatePicker!
    @IBOutlet weak var btnToday: UIBarButtonItem!
    @IBOutlet weak var btnAll: UIButton!
    
    var delegate: SelectDateViewControllerDelegate?
    var selectedDate: Date?
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let _ = self.delegate as? GenerateReportViewController {
            let def = UserDefaults.standard
            self.selectDatePicker.minimumDate = def.value(forKey: StockTakeInfoList.dateMin.rawValue) as? Date
            self.selectDatePicker.maximumDate = def.value(forKey: StockTakeInfoList.dateMax.rawValue) as? Date
            self.btnAll.isHidden = false
            
            let df = DateFormatter()
            df.dateFormat = "dd/MM/yyyy"
            if df.string(from: self.selectDatePicker.maximumDate!) == df.string(from: Date()) {
                self.btnToday.isEnabled = true
            } else {
                self.btnToday.isEnabled = false
            }
            
        } else {
            self.selectDatePicker.maximumDate = Date()
            self.btnAll.isHidden = true
            self.btnToday.isEnabled = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func barBtnDone(_ sender: Any) {
        if let _ = self.delegate {
            self.delegate?.didSelectDate(self.selectDatePicker, selectedDate: self.selectDatePicker.date)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func barBtnToday(_ sender: Any) {
        selectDatePicker.date = Date()
    }
    
    @IBAction func btnAllPressed(_ sender: Any) {
        if let vc = self.delegate as? GenerateReportViewController {
            vc.selectedDateString = "All"
            vc.btnSelectedDate.setTitle("All", for: .normal)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
