//
//  StockTakeOperationViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 27/03/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// MARK: STOCK TAKE OPERATION
class StockTakeOperationViewController: UITableViewController, UITextFieldDelegate {
    
    // MARK: Outlets
    let ROW_QTY_ONE = 0
    let ROW_STOCK_ID = 1
    let ROW_STOCK_QTY = 2
    let ROW_SAVE = 3
    
    @IBOutlet weak var barBtnScanBarcode: UIBarButtonItem!
    @IBOutlet weak var lblAutoSave: UILabel!
    @IBOutlet weak var txtStockID: UITextField!
    @IBOutlet weak var txtStockQuantity: UITextField!
    @IBOutlet weak var lblStockTakeItemError: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblSaveRecord: UILabel!
    @IBOutlet weak var lblSaveDetail: UILabel!
    @IBOutlet weak var lblNoSaveRecord: UILabel!
    
    var branch: String?
    var selectedDate: Date?
    var location: String?
    var shelf: String?
    var user: String?
    var selectedDateString: String?
    
    var isQtyOne: Bool?
    var currentTextField: UITextField?
    var saveCount: Int = 0
    var recordCount: Int?
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtStockID.delegate = self
        self.txtStockQuantity.delegate = self
        
        self.lblStockTakeItemError.text = ""
        self.lblSaveRecord.isHidden = true
        self.lblSaveDetail.isHidden = true
        self.lblNoSaveRecord.isHidden = true
        
        self.txtStockID.becomeFirstResponder()
        activateButton(for: self.btnSave, to: false)
        
        switch self.isQtyOne! {
        case true:
            self.lblAutoSave.isHidden = false
            self.txtStockQuantity.isEnabled = false
            self.txtStockQuantity.text = "1"
        case false:
            self.lblAutoSave.isHidden = true
            self.txtStockQuantity.isEnabled = true
            self.txtStockQuantity.text = ""
        }
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        do {
            let countStockTake = try context.count(for:StockTakeRecord.fetchRequest())
            self.saveCount = countStockTake
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
        // Tap anywhere to dismiss the keyboard (when qty is not 1).
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        if self.isQtyOne! != true {
            view.addGestureRecognizer(tap)
        }
        
        //--- add UIToolBar on keyboard and Done button on UIToolBar ---//
        self.addDoneButtonOnKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ScannerViewController {
            vc.preferredContentSize = CGSize(width: 400, height: 400)
            vc.delegate = self
            
            // Auto-save feature: Stock ID textfield is always active.
            if self.txtStockID.isEditing == true {
                self.txtStockID.resignFirstResponder()
            }
        }
    }
    
    // set height for each cell row (hard-coded)
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case ROW_QTY_ONE:
            return self.isQtyOne == true ? 50: 30
        case ROW_STOCK_ID:
            return 45
        case ROW_STOCK_QTY:
            return 45
        case ROW_SAVE:
            return 150
        default:
//            return UITableViewAutomaticDimension
            return 44
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activateButton(for: self.btnSave, to: false)
        self.currentTextField = textField
        self.lblStockTakeItemError.text = ""
//        self.lblSaveRecord.isHidden = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let intStockQuantity = Int(self.txtStockQuantity.text!) {
            // Remove leading zeros of Quantity string, i.e.: 00100 to 100
            self.txtStockQuantity.text = String(intStockQuantity)
        }
        
        if self.txtStockQuantity.text != "" && self.txtStockID.text != "" {
            activateButton(for: self.btnSave, to: true)
        } else {
            activateButton(for: self.btnSave, to: false)
        }
        
        if self.lblSaveRecord.isHidden == false {
            self.lblSaveRecord.isHidden = true
        }
    }
    
    @IBAction func btnSaveStockTake(_ sender: Any) {
        // Dismiss keyboard when pressed
        if let endEditTextField = self.currentTextField {
            endEditTextField.resignFirstResponder()
        }
        
        // Check Stock Qty
        if let validQty = Int(self.txtStockQuantity.text!) {
            if validQty > 0 {
                self.lblStockTakeItemError.text = ""
            } else {
                self.lblStockTakeItemError.text = "Stock Quantity must be greater than 0."
            }
        } else {
            self.lblStockTakeItemError.text = "Stock Quantity must contain number 0-9 only."
        }
        
        // Show error, otherwise, save data
        if self.lblStockTakeItemError.text != "" {
            self.lblStockTakeItemError.isHidden = false
            self.lblSaveRecord.isHidden = true
        } else {
            saveStockTakeData()
            self.txtStockID.text = ""
            if self.isQtyOne == false {
                self.txtStockQuantity.text = ""
            }
        }
    }
    
    // selector to dismiss keyboard
    @objc func dismissKeyboard() {
//        view.endEditing(true)
    }
    
    // MARK: Core Data
    func saveStockTakeData() {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        
        // Save record into Entity: StockTakeRecord
        let savedStockTakeRecord = StockTakeRecord(context: context)
        
        savedStockTakeRecord.branch = self.branch
        savedStockTakeRecord.dateStockTake = self.selectedDate
        savedStockTakeRecord.location = self.location
        savedStockTakeRecord.shelf = self.shelf
        savedStockTakeRecord.user = self.user
        savedStockTakeRecord.itemCode = self.txtStockID.text!
        savedStockTakeRecord.itemQty = Int64(self.txtStockQuantity.text!)!
        savedStockTakeRecord.dateSaved = Date()
        
        self.commit()
        
        // Stock Take Info List created for report viewing
        let def = UserDefaults.standard
        
        var branchList = def.stringArray(forKey: StockTakeInfoList.branchList.rawValue) ?? [String]()
        var dateList = def.stringArray(forKey: StockTakeInfoList.dateList.rawValue) ?? [String]()
        var locationList = def.stringArray(forKey: StockTakeInfoList.locationList.rawValue) ?? [String]()
        var shelfList = def.stringArray(forKey: StockTakeInfoList.shelfList.rawValue) ?? [String]()
        var userList = def.stringArray(forKey: StockTakeInfoList.userList.rawValue) ?? [String]()
        
        let minDate = def.value(forKey: StockTakeInfoList.dateMin.rawValue) as? Date
        let maxDate = def.value(forKey: StockTakeInfoList.dateMax.rawValue) as? Date
        
        // Add branch into list
        if branchList.contains(self.branch!) == false {
            branchList.append(self.branch!)
            def.set(branchList, forKey: StockTakeInfoList.branchList.rawValue)
        }
        
        // Add date into list
        if dateList.contains(self.selectedDateString!) == false { // this line has problem
            dateList.append(self.selectedDateString!)
            def.set(dateList, forKey: StockTakeInfoList.dateList.rawValue)
        }
        
        // Add location into list
        if locationList.contains(self.location!) == false {
            locationList.append(self.location!)
            def.set(locationList, forKey: StockTakeInfoList.locationList.rawValue)
        }
        
        // Add shelf into list
        if shelfList.contains(self.shelf!) == false {
            shelfList.append(self.shelf!)
            def.set(shelfList, forKey: StockTakeInfoList.shelfList.rawValue)
        }
        
        // Add user into list
        if userList.contains(self.user!) == false {
            userList.append(self.user!)
            def.set(userList, forKey: StockTakeInfoList.userList.rawValue)
        }
        
        // Set min Date
        if let _ = minDate {
            if minDate! > self.selectedDate! {
                def.set(self.selectedDate, forKey: StockTakeInfoList.dateMin.rawValue)
            }
        } else {
            def.set(self.selectedDate, forKey: StockTakeInfoList.dateMin.rawValue)
        }
        
        // Set max Date
        if let _ = maxDate {
            if maxDate! < self.selectedDate! {
                def.set(self.selectedDate, forKey: StockTakeInfoList.dateMax.rawValue)
            }
        } else {
            def.set(self.selectedDate, forKey: StockTakeInfoList.dateMax.rawValue)
        }
        
        def.synchronize()
        
        // Print stock take info
        print(self.branch!)
        printDate(for: self.selectedDate!, time: false)
        print(self.location!)
        print(self.shelf!)
        print(self.user!)
        print(self.txtStockID.text!)
        print(self.txtStockQuantity.text!)
        printDate(for: Date(), time: true)
        print()
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        print(paths[0])
        
        self.saveCount += 1
        
        self.lblSaveRecord.isHidden = false
        
        self.lblSaveDetail.text = "Stock ID: " + self.txtStockID.text! + " | Qty: " + self.txtStockQuantity.text!
        self.lblSaveDetail.isHidden = false
        
        if self.saveCount == 1 {
            self.lblNoSaveRecord.text = "Total no of Stock Take record: " + String(saveCount)
        } else {
            self.lblNoSaveRecord.text = "Total no of Stock Take records: " + String(saveCount)
        }
        
//        if self.saveCount == 1 {
//            self.lblNoSaveRecord.text = "There is " + String(saveCount) + " record saved."
//        } else {
//            self.lblNoSaveRecord.text = "There are " + String(saveCount) + " records saved."
//        }
        
        self.lblNoSaveRecord.isHidden = false
        
        self.txtStockID.becomeFirstResponder()
    }
    
    // Add Done button to the keyboard.
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(StockTakeOperationViewController.doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.txtStockQuantity.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.currentTextField!.resignFirstResponder()
    }
    
}
