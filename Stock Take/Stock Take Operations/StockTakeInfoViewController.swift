//
//  StockTakeInfoViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 16/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// MARK: STOCK TAKE OPERATION BY BATCH
class StockTakeInfoViewController: UITableViewController, UITextFieldDelegate {
    
    // MARK: Outlets
    @IBOutlet weak var txtBranchStockTake: UITextField!
    @IBOutlet weak var btnSelectDateStockTake: UIButton!
    @IBOutlet weak var txtLocationStockTake: UITextField!
    @IBOutlet weak var txtShelfStockTake: UITextField!
    @IBOutlet weak var txtUserStockTake: UITextField!
    @IBOutlet weak var switchQtyOne: UISwitch!
    @IBOutlet weak var lblStockTakeError: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    
    var selectedDate: Date?
    var selectedDateString: String?
    var isQtyOne: Bool?
    var currentTextField: UITextField?
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtBranchStockTake.delegate = self
        self.txtLocationStockTake.delegate = self
        self.txtShelfStockTake.delegate = self
        self.txtUserStockTake.delegate = self
        
        self.lblStockTakeError.text = ""
        
        self.switchQtyOne.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
        
        // Tap anywhere to dismiss the keyboard.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // Default date is current date
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        self.selectedDate = Date()
        
        self.selectedDateString = df.string(from: Date())
        
        self.btnSelectDateStockTake.setTitle("\(df.string(from: self.selectedDate!))", for: .normal)
        
        // Load switch status
        let def = UserDefaults.standard
        if let isOn = def.value(forKey: CurrentStockTakeInfo.currentIsQtyOne.rawValue) {
            self.switchQtyOne.isOn = isOn as! Bool
        } else {
            def.set(false, forKey: CurrentStockTakeInfo.currentIsQtyOne.rawValue)
            def.synchronize()
            self.switchQtyOne.isOn = false
        }
        
        // Load Stock Take Information
        self.loadStockTakeInfo()
        activateButton(for: self.btnConfirm, to: checkConfirmStatus())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nvc = segue.destination as? UINavigationController {
            if let vc = nvc.viewControllers[0] as? SelectDateViewController {
                vc.preferredContentSize = CGSize(width: 300, height: 300)
                vc.delegate = self
                
                if let _ = self.selectedDate {
                    vc.selectedDate = self.selectedDate
                } else {
                    vc.selectedDate = Date()
                }
            }
        } else if let vc = segue.destination as? StockTakeOperationViewController {
            if let _ = self.isQtyOne {
                vc.isQtyOne = self.isQtyOne
            } else {
                vc.isQtyOne = false
            }
            vc.branch = self.txtBranchStockTake.text
            vc.selectedDate = self.selectedDate
            vc.selectedDateString = self.selectedDateString
            vc.location = self.txtLocationStockTake.text
            vc.shelf = self.txtShelfStockTake.text
            vc.user = self.txtUserStockTake.text
//            vc.batchNo = self.batchNo
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activateButton(for: self.btnConfirm, to: false)
        self.currentTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let def = UserDefaults.standard
        
        switch textField {
        case self.txtBranchStockTake:
            def.set(self.txtBranchStockTake.text, forKey: CurrentStockTakeInfo.currentBranch.rawValue)
        case self.txtLocationStockTake:
            def.set(self.txtLocationStockTake.text, forKey: CurrentStockTakeInfo.currentLocation.rawValue)
        case self.txtShelfStockTake:
            def.set(self.txtShelfStockTake.text, forKey: CurrentStockTakeInfo.currentShelf.rawValue)
        case self.txtUserStockTake:
            def.set(self.txtUserStockTake.text, forKey: CurrentStockTakeInfo.currentUser.rawValue)
        default:
            break
        }
        
        def.synchronize()
        activateButton(for: self.btnConfirm, to: checkConfirmStatus())
    }
    
    @IBAction func btnConfirmStockTake(_ sender: Any) {
        self.isQtyOne = self.switchQtyOne.isOn
    }
    
    func loadStockTakeInfo() {
        let def = UserDefaults.standard
        let branch: String? = def.value(forKey: CurrentStockTakeInfo.currentBranch.rawValue) as? String
        let location: String? = def.value(forKey: CurrentStockTakeInfo.currentLocation.rawValue) as? String
        let shelf: String? = def.value(forKey: CurrentStockTakeInfo.currentShelf.rawValue) as? String
        let user: String? = def.value(forKey: CurrentStockTakeInfo.currentUser.rawValue) as? String
        
        if let _ = branch {
            self.txtBranchStockTake.text = branch
        } else {
            self.txtBranchStockTake.text = ""
        }
        
        if let _ = location {
            self.txtLocationStockTake.text = location
        } else {
            self.txtLocationStockTake.text = ""
        }
        
        if let _ = shelf {
            self.txtShelfStockTake.text = shelf
        } else {
            self.txtShelfStockTake.text = ""
        }
        
        if let _ = user {
            self.txtUserStockTake.text = user
        } else {
            self.txtUserStockTake.text = ""
        }
    }
    
    func checkConfirmStatus() -> Bool {
        if self.txtBranchStockTake.text != "" && self.btnSelectDateStockTake.currentTitle != "Select date" && self.txtLocationStockTake.text != "" && self.txtShelfStockTake.text != "" && self.txtUserStockTake.text != "" {
            return true
        } else {
            return false
        }
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let def = UserDefaults.standard
        def.set(mySwitch.isOn, forKey: CurrentStockTakeInfo.currentIsQtyOne.rawValue)
        def.synchronize()
        self.dismissKeyboard()
    }
    
    // selector to dismiss keyboard
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

// MARK: SelectDateViewControllerDelegate
extension StockTakeInfoViewController: SelectDateViewControllerDelegate {
    func didSelectDate(_ sender: UIDatePicker, selectedDate: Date) {
        // Set date
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        self.selectedDate = selectedDate
        self.btnSelectDateStockTake.setTitle("\(df.string(from: selectedDate))", for: .normal)
        self.selectedDateString = df.string(from: selectedDate)
        
        activateButton(for: self.btnConfirm, to: checkConfirmStatus())
    }
}

