//
//  PriceCheckViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 23/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// MARK: STOCK TAKE OPERATION
class PriceCheckViewController: UITableViewController, UITextFieldDelegate {
    
    // MARK: Outlets
    @IBOutlet weak var txtScanProduct: UITextField!
    @IBOutlet weak var lblItemDescription: UILabel!
    
    @IBOutlet weak var lblArmsCode: UILabel!
    @IBOutlet weak var lblMcode: UILabel!
    @IBOutlet weak var lblArtno: UILabel!
    @IBOutlet weak var lblBarcode: UILabel!
    @IBOutlet weak var lblUomFraction: UILabel!
    
    @IBOutlet weak var viewPrice: UIView!
    @IBOutlet weak var viewTax: UIStackView!
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceBeforeTax: UILabel!
    @IBOutlet weak var lblTaxAmount: UILabel!
    
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblVendor: UILabel!
    @IBOutlet weak var lblSkuType: UILabel!
    @IBOutlet weak var lblPriceType: UILabel!
    
    @IBOutlet weak var lblDbDate: UILabel!
    @IBOutlet weak var lblSKURecords: UILabel!
    
    let IMPORT_DATE = "importDate"
    let isTaxZeroRate = true
    
    var scannedProduct: SKUItem?
    var code: String?
    var skuCount: Int = 0
    
    var armsCodeListMain: [String]?
    var artnoListMain: [String]?
    var mcodeListMain: [String]?
    var barcodeListMain: [String]?
    
    var skuItemsMain: [SKUItem]?
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtScanProduct.delegate = self
        shouldHideProductDetails(is: true)
        
        self.lblPriceType.isHidden = true
        if self.isTaxZeroRate == true {
            self.viewTax.isHidden = true
        }
        
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        let def = UserDefaults.standard
        let importDate = def.value(forKey: IMPORT_DATE) as? String
        if let _ = importDate {
            self.lblDbDate.text = "Database imported on: " + importDate!
            self.lblDbDate.isHidden = false
        } else {
            self.lblDbDate.text = "-"
            self.lblDbDate.isHidden = true
        }
        
        // Fetch imported SKU items when Price Check is selected
        fetchSKUItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let _ = self.code {
            searchItem()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ScannerViewController {
            vc.preferredContentSize = CGSize(width: 400, height: 400)
            vc.delegate = self
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.code = self.txtScanProduct.text
        
        // search code only when text field is not empty
        if let code = self.code {
            if code != "" {
                searchItem()
            }
        }
    }
    
    func fetchSKUItems() {
        var armsCodeList: [String]?
        var artnoList: [String]?
        var mcodeList: [String]?
        var barcodeList: [String]?
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchSKUItem: NSFetchRequest<SKUItem> = SKUItem.fetchRequest()
        var skuItems: [SKUItem]?
        do {
            skuItems = try context.fetch(fetchSKUItem as! NSFetchRequest<NSFetchRequestResult>) as? [SKUItem]
        } catch {
            fatalError("Failed to fetch SKU items: \(error)")
        }
        
        self.skuItemsMain = skuItems
        
//        print(skuItems!.count)
//        print(self.code!)
        
        for item in skuItems! {
            if let _ = armsCodeList {
                armsCodeList?.append(item.armsCode!)
            } else {
                armsCodeList = [item.armsCode!]
            }
            
            if let _ = artnoList {
                artnoList?.append(item.artno!)
            } else {
                artnoList = [item.artno!]
            }
            
            if let _ = mcodeList {
                mcodeList?.append(item.mcode!)
            } else {
                mcodeList = [item.mcode!]
            }
            
            if let _ = barcodeList {
                barcodeList?.append(item.barcode!)
            } else {
                barcodeList = [item.barcode!]
            }
        }
        
        self.armsCodeListMain = armsCodeList
        self.mcodeListMain = mcodeList
        self.artnoListMain = artnoList
        self.barcodeListMain = barcodeList
        
        if let list = self.armsCodeListMain {
            self.lblSKURecords.text = "No of SKU records: " + String(list.count)
        } else {
            self.lblSKURecords.text = "There is no SKU records. Please import\nSKU data before proceeding to Price Check."
        }
//        print(barcodeList!.count)
    }
    
    func searchItem() {
        
        var skuItems = self.skuItemsMain
        
        if self.armsCodeListMain?.contains(self.code!) == true {
            for item in skuItems! {
                if self.code! == item.armsCode! {
                    self.scannedProduct = item
                    break
                }
            }
        } else if self.artnoListMain?.contains(self.code!) == true {
            for item in skuItems! {
                if self.code! == item.artno! {
                    self.scannedProduct = item
                    break
                }
            }
        } else if self.mcodeListMain?.contains(self.code!) == true {
            for item in skuItems! {
                if self.code! == item.mcode! {
                    self.scannedProduct = item
                    break
                }
            }
        } else if self.barcodeListMain?.contains(self.code!) == true {
            for item in skuItems! {
                if self.code! == item.barcode! {
                    self.scannedProduct = item
                    break
                }
            }
        } else {
            self.scannedProduct = nil
        }
        showItem()
        
        skuItems?.removeAll()
    }
    
    func showItem() {
        if let product = self.scannedProduct {
            let sList = ["SR", "DS", "AJS"]
            let zList = ["ZRL", "ZRE", "OS", "RS", "GS"]
            let eList = ["ES43", "ES"]
            
            var mcode: String?
            var artno: String?
            var barcode: String?
            
            var category: String?
            var brand: String?
            var vendor: String?
            var skuType: String?
            var priceType: String?
            
            mcode = checkString(product.mcode)
            artno = checkString(product.artno)
            barcode = checkString(product.barcode)
            
            category = checkString(product.category)
            brand = checkString(product.brand)
            vendor = checkString(product.vendor)
            skuType = checkString(product.skuType)
            priceType = checkString(product.priceType)
            
            var taxCode: String?
            var taxRate: Double?
            
            var price: Double?
            var priceBeforeTax: Double?
            var taxAmount: Double?
            
            for element in sList {
                if self.scannedProduct?.outputTax?.range(of:element) != nil {
                    taxCode = "S"
                    taxRate = 6.00
//                    taxRate = 0.00  // From 1st June
                    break
                }
            }
            
            if taxCode == nil || taxRate == nil {
                for element in zList {
                    if self.scannedProduct?.outputTax?.range(of:element) != nil {
                        taxCode = "Z"
                        taxRate = 0.00
                        break
                    }
                }
            }
            
            if taxCode == nil || taxRate == nil {
                for element in eList {
                    if self.scannedProduct?.outputTax?.range(of:element) != nil {
                        taxCode = "E"
                        taxRate = 0.00
                        break
                    }
                }
            }
            
            // Starting from June 1st
            if isTaxZeroRate == true {
                price = self.scannedProduct?.sellingPrice
                priceBeforeTax = price!
                taxAmount = 0.00
            }
            // before June 1st
            else {
                if (self.scannedProduct?.inclusiveTax)! {
                    price = self.scannedProduct?.sellingPrice
                    priceBeforeTax = price! / ( 1.00 + ( taxRate! / 100.00 ) )
                } else {
                    priceBeforeTax = self.scannedProduct?.sellingPrice
                    price = priceBeforeTax! + ( priceBeforeTax! * taxRate! / 100.00 )
                }
                taxAmount = priceBeforeTax! * ( taxRate! / 100.00 )
            }
            
            self.lblArmsCode.text = "ARMS Code: " + product.armsCode!
            self.lblMcode.text = "MCode: " + mcode!
            self.lblArtno.text = "Artno: " + artno!
            self.lblBarcode.text = "Barcode: " + barcode!
            
            self.lblItemDescription.text = product.productDescription!
            self.lblUomFraction.text = product.uomCode! + " (" + String(product.fraction) + ")"
            
            self.lblPrice.text = "RM " + String(format: "%.2f", price!)
            
            self.lblPriceBeforeTax.text = "Before GST: " + String(format: "%.2f", priceBeforeTax!)
            self.lblTaxAmount.text = "GST (" + taxCode! + "): " + String(format: "%.2f", taxAmount!)
            
            self.lblDepartment.text = "Department: " + product.department!
            self.lblCategory.text = "Category: " + category!
            self.lblBrand.text = "Brand: " + brand!
            self.lblVendor.text = "Vendor: " + vendor!
            self.lblSkuType.text = "SKU Type: " + skuType!
            
            if let _ = priceType {
                if priceType! == "-" {
                    self.lblPriceType.isHidden = true
                } else {
                    self.lblPriceType.isHidden = false
                    self.lblPriceType.text = "Price Type: " + priceType! // show price type only when sku type is consignment
                }
            }
            
            // Show product details.
            shouldHideProductDetails(is: false)
            
        }
        else {
            let alertController = UIAlertController(title: "Error", message: "Scanned code: \(self.code!)\nItem not found." , preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "Close", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func shouldHideProductDetails(is firstTime: Bool) {
        self.lblArmsCode.isHidden = firstTime
        self.lblMcode.isHidden = firstTime
        self.lblArtno.isHidden = firstTime
        self.lblBarcode.isHidden = firstTime
        
        self.lblItemDescription.isHidden = firstTime
        self.lblUomFraction.isHidden = firstTime
        self.viewPrice.isHidden = firstTime
        
        self.lblDepartment.isHidden = firstTime
        self.lblCategory.isHidden = firstTime
        self.lblBrand.isHidden = firstTime
        self.lblVendor.isHidden = firstTime
        self.lblSkuType.isHidden = firstTime
    }
    
    func checkString(_ str: String?) -> String {
        if str == nil || str == "" {
            return "-"
        } else {
            return str!
        }
    }
}
