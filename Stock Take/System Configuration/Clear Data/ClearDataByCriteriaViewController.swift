//
//  ClearDataByCriteriaViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 19/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol ClearDataByCriteriaViewControllerDelegate{
    func didSelectCriteria(_ sender: Any, selectedBatchNo: String)
}

// MARK: Select Criteria View Controller
class ClearDataByCriteriaViewController: UITableViewController {
    
    // MARK: Outlets
    @IBOutlet weak var barBtnDelete: UIBarButtonItem!
    
    lazy var elementList: [String]? = {
        let element = [String]()
        return element
    }()
    
    var criteria: criteriaType?
    var selectedElement: String?
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.barBtnDelete.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if let elementList = self.elementList {
            count = elementList.count
        }
        return count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if let element = self.elementList?[indexPath.row] {
            cell.textLabel?.text = element
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let element = self.elementList?[indexPath.row] {
            self.selectedElement = element
        }
        self.barBtnDelete.isEnabled = true
    }
    
    @IBAction func onBarBtnDeleteTapped(_ sender: Any) {
        
        var stringCriteria: String?
        
        switch self.criteria {
        case .branch?:
            stringCriteria = "branch"
        case .date?:
            stringCriteria = "date"
        case .location?:
            stringCriteria = "location"
        case .shelf?:
            stringCriteria = "shelf"
        case .user?:
            stringCriteria = "user"
        default:
            stringCriteria = ""
        }
        
        // Action: Delete
        let deleteAction = UIAlertAction(title: "Clear", style: .destructive, handler: { action in
            // Action: Confirm delete
            let action = UIAlertAction(title: "OK", style: .default, handler: { action in
                let hasRecord = self.clearDataByCriteria()
                if hasRecord {
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    // Action: Back to Settings menu
                    let actionBackToRoot = UIAlertAction(title: "OK", style: .default, handler: {action in
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                    // Alert 3: No Stock Take Record
                    let acBackToRoot = UIAlertController(title: "No Stock Take record", message: "Database is now empty.", preferredStyle: .alert)
                    acBackToRoot.addAction(actionBackToRoot)
                    
                    self.present(acBackToRoot, animated: true, completion: nil)
                }
            })
            // Alert 2: Clear records
            let alertController = UIAlertController(title: "Clear records", message: "Stock Take records for selected \(stringCriteria!) have been cleared." , preferredStyle: .alert)
            alertController.addAction(action)
            
            self.present(alertController, animated: true, completion: nil)
        })
        // Action: Cancel
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        // Alert 1: Are you sure?
        let acConfirm = UIAlertController(title: "Are you sure?", message: "This action cannot be undone.", preferredStyle: .alert)
        acConfirm.addAction(cancelAction)
        acConfirm.addAction(deleteAction)
        
        self.present(acConfirm, animated: true, completion: nil)
    }
    
    func clearDataByCriteria() -> Bool {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchStockTakeRecord: NSFetchRequest<StockTakeRecord> = StockTakeRecord.fetchRequest()
        var stockTakeRecords: [StockTakeRecord]?
        
        do {
            stockTakeRecords = try context.fetch(fetchStockTakeRecord as! NSFetchRequest<NSFetchRequestResult>) as? [StockTakeRecord]
            for item in stockTakeRecords! {
                let objectData: NSManagedObject = item as NSManagedObject
                
                switch self.criteria {
                case .branch?:
                    if item.branch == self.selectedElement {
                        context.delete(objectData)
                    }
                case .date?:
                    let df = DateFormatter()
                    df.dateFormat = "dd/MM/yyyy"
                    if df.string(from: item.dateStockTake!) == self.selectedElement {
                        context.delete(objectData)
                    }
                case .location?:
                    if item.location == self.selectedElement {
                        context.delete(objectData)
                    }
                case .shelf?:
                    if item.branch == self.selectedElement {
                        context.delete(objectData)
                    }
                case .user?:
                    if item.user == self.selectedElement {
                        context.delete(objectData)
                    }
                default:
                    break
                }
            }
        } catch {
            fatalError("Failed to fetch Stock Take Item: \(error)")
        }
        self.commit()
        
        // update UserDefaults
        let def = UserDefaults.standard
        
        var branchList = [String]()
        var minDate: Date?
        var maxDate: Date?
        var dateList = [String]()
        var locationList = [String]()
        var shelfList = [String]()
        var userList = [String]()
        
        // Fetch again from database
        do {
            stockTakeRecords = try context.fetch(fetchStockTakeRecord as! NSFetchRequest<NSFetchRequestResult>) as? [StockTakeRecord]
            for item in stockTakeRecords! {
                if branchList.contains(item.branch!) == false {
                    branchList.append(item.branch!)
                }
                
                let df = DateFormatter()
                df.dateFormat = "dd/MM/yyyy"
                if dateList.contains(df.string(from: item.dateStockTake!)) == false {
                    dateList.append(df.string(from: item.dateStockTake!))
                }
                
                if locationList.contains(item.location!) == false {
                    locationList.append(item.location!)
                }
                
                if shelfList.contains(item.shelf!) == false {
                    shelfList.append(item.shelf!)
                }
                
                if userList.contains(item.user!) == false {
                    userList.append(item.user!)
                }
                
                if let _ = minDate {
                    if minDate! > item.dateStockTake! {
                        minDate = item.dateStockTake
                    }
                } else {
                    minDate = item.dateStockTake
                }
                
                if let _ = maxDate {
                    if maxDate! < item.dateStockTake! {
                        maxDate = item.dateStockTake
                    }
                } else {
                    maxDate = item.dateStockTake
                }
            }
        } catch {
            fatalError("Failed to fetch Stock Take Item: \(error)")
        }
        
        def.set(branchList, forKey: StockTakeInfoList.branchList.rawValue)
        def.set(dateList, forKey: StockTakeInfoList.dateList.rawValue)
        def.set(locationList, forKey: StockTakeInfoList.locationList.rawValue)
        def.set(shelfList, forKey: StockTakeInfoList.shelfList.rawValue)
        def.set(userList, forKey: StockTakeInfoList.userList.rawValue)
        
        def.set(minDate, forKey: StockTakeInfoList.dateMin.rawValue)
        def.set(maxDate, forKey: StockTakeInfoList.dateMax.rawValue)
        
        def.synchronize()
        
        if (stockTakeRecords?.count)! > 0 {
            return true
        } else {
            return false
        }
    }
}

