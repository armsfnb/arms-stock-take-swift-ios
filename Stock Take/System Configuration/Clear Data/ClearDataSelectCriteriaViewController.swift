//
//  ClearDataSelectCriteriaViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 19/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// MARK: STOCK TAKE OPERATION BY BATCH
class ClearDataSelectCriteriaViewController: UITableViewController {
    
    // MARK: Outlets
    let ROW_BRANCH = 0
    let ROW_DATE = 1
    let ROW_LOCATION = 2
    let ROW_SHELF = 3
    let ROW_USER = 4
    
    var delegate: ClearDataViewController?
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case ROW_BRANCH:
            self.delegate?.selectedCriteria = .branch
            self.delegate?.btnCriteria.setTitle("Branch", for: .normal)
        case ROW_DATE:
            self.delegate?.selectedCriteria = .date
            self.delegate?.btnCriteria.setTitle("Date", for: .normal)
        case ROW_LOCATION:
            self.delegate?.selectedCriteria = .location
            self.delegate?.btnCriteria.setTitle("Location", for: .normal)
        case ROW_SHELF:
            self.delegate?.selectedCriteria = .shelf
            self.delegate?.btnCriteria.setTitle("Shelf", for: .normal)
        case ROW_USER:
            self.delegate?.selectedCriteria = .user
            self.delegate?.btnCriteria.setTitle("User", for: .normal)
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBarBtnCancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
