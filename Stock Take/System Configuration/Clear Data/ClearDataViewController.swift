//
//  ClearDataViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 19/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// MARK: STOCK TAKE OPERATION BY BATCH
class ClearDataViewController: UITableViewController {
    
    // MARK: Outlets
    @IBOutlet weak var switchClearAll: UISwitch!
    @IBOutlet weak var lblClearDateBy: UILabel!
    @IBOutlet weak var btnCriteria: UIButton!
    
    var isClearAll: Bool?
    var selectedCriteria: criteriaType?
    var identifier = "ClearDataByCriteria"
    
    lazy var elementList: [String]? = {
        let element = [String]()
        return element
    }()

    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isClearAll = true
        self.lblClearDateBy.isHidden = true
        self.btnCriteria.isHidden = true
        
        self.selectedCriteria = .branch
        
        self.switchClearAll.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.identifier {
            if let vc = segue.destination as? ClearDataByCriteriaViewController {
                self.loadElementList()
                switch self.selectedCriteria {
                case .branch?:
                    vc.navigationItem.title = "Select Branch"
                case .date?:
                    vc.navigationItem.title = "Select Date"
                case .location?:
                    vc.navigationItem.title = "Select Location"
                case .shelf?:
                    vc.navigationItem.title = "Select Shelf"
                case .user?:
                    vc.navigationItem.title = "Select User"
                default:
                    break
                }
                vc.elementList = self.elementList
                vc.criteria = self.selectedCriteria
            }
        } else {
            if let nvc = segue.destination as? UINavigationController {
                if let vc = nvc.viewControllers[0] as? ClearDataSelectCriteriaViewController {
                    // select Branch/Date/Location/Shelf/User
                    vc.preferredContentSize = CGSize(width: 300, height: 300)
                    vc.delegate = self
                }
            }
        }
    }
    
    @IBAction func onConfirmButtonTapped(_ sender: Any) {
        if self.isClearAll == true {
            // Action: Clear
            let deleteAction = UIAlertAction(title: "Clear", style: .destructive, handler: { action in
                // Action: Confirm clear all, back to Settings view
                let action = UIAlertAction(title: "OK", style: .default, handler: { action in
                    self.clearAll()
                    
                    let resetAction = UIAlertAction(title: "Reset", style: .destructive, handler: { action in
                        self.resetStockTakeSettings()
                        self.navigationController?.popViewController(animated: true)
                    })
                    let cancelResetAction = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    let resetController = UIAlertController(title: "Reset Stock Take Settings?", message: "This action cannot be undone.", preferredStyle: .alert)
                    resetController.addAction(cancelResetAction)
                    resetController.addAction(resetAction)
                    
                    self.present(resetController, animated: true, completion: nil)
                    
                })
                // Alert 2: Clear all
                let alertController = UIAlertController(title: "Clear All", message: "All Stock Take records have been cleared." , preferredStyle: .alert)
                alertController.addAction(action)
                
                self.present(alertController, animated: true, completion: nil)
            })
            // Action: Cancel
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            // Alert 1: Confirm delete
            let acConfirm = UIAlertController(title: "Are you sure?", message: "This action cannot be undone.", preferredStyle: .alert)
            acConfirm.addAction(cancelAction)
            acConfirm.addAction(deleteAction)
            
            self.present(acConfirm, animated: true, completion: nil)
            
        } else {
            self.performSegue(withIdentifier: self.identifier, sender: sender as? UIButton)
        }
    }
    
    // Load Data
    func loadElementList() {
        let def = UserDefaults.standard

        let branchList = def.stringArray(forKey: StockTakeInfoList.branchList.rawValue) ?? [String]()
        let dateList = def.stringArray(forKey: StockTakeInfoList.dateList.rawValue) ?? [String]()
        let locationList = def.stringArray(forKey: StockTakeInfoList.locationList.rawValue) ?? [String]()
        let shelfList = def.stringArray(forKey: StockTakeInfoList.shelfList.rawValue) ?? [String]()
        let userList = def.stringArray(forKey: StockTakeInfoList.userList.rawValue) ?? [String]()

        switch self.selectedCriteria {
        case .branch?:
            self.elementList = branchList
        case .date?:
            self.elementList = dateList
        case .location?:
            self.elementList = locationList
        case .shelf?:
            self.elementList = shelfList
        case .user?:
            self.elementList = userList
        default:
            break
        }
        
        // sort date
        if self.selectedCriteria == .date {
            var convertedDateList = [Date]()
            
            let df = DateFormatter()
            df.dateFormat = "dd/MM/yyyy"
            
            for dat in dateList {
                let date = df.date(from: dat)
                if let newDate = date {
                    convertedDateList.append(newDate)
                }
            }
            self.elementList?.removeAll()
            
            let newDates = convertedDateList.sorted { $0 > $1 }
            for newDate in newDates {
                self.elementList?.append(df.string(from: newDate))
            }
            
        } else {
            self.elementList?.sort()
        }
    }
    
    // Clear Data
    func clearAll() {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchStockTakeRecord: NSFetchRequest<StockTakeRecord> = StockTakeRecord.fetchRequest()
        var stockTakeRecords: [StockTakeRecord]?
        do {
            stockTakeRecords = try context.fetch(fetchStockTakeRecord as! NSFetchRequest<NSFetchRequestResult>) as? [StockTakeRecord]
            for item in stockTakeRecords! {
                
                let objectData: NSManagedObject = item as NSManagedObject
                context.delete(objectData)
            }
        } catch {
            fatalError("Failed to fetch Stock Take Item: \(error)")
        }
        self.commit()
        
        let def = UserDefaults.standard
        
        def.set(nil, forKey: StockTakeInfoList.branchList.rawValue)
        def.set(nil, forKey: StockTakeInfoList.dateList.rawValue)
        def.set(nil, forKey: StockTakeInfoList.locationList.rawValue)
        def.set(nil, forKey: StockTakeInfoList.shelfList.rawValue)
        def.set(nil, forKey: StockTakeInfoList.userList.rawValue)
        def.set(nil, forKey: StockTakeInfoList.dateMin.rawValue)
        def.set(nil, forKey: StockTakeInfoList.dateMax.rawValue)
        
        def.synchronize()
    }
    
    func resetStockTakeSettings() {
        let def = UserDefaults.standard
        
        def.set(nil, forKey: CurrentStockTakeInfo.currentBranch.rawValue)
        def.set(nil, forKey: CurrentStockTakeInfo.currentDate.rawValue)
        def.set(nil, forKey: CurrentStockTakeInfo.currentLocation.rawValue)
        def.set(nil, forKey: CurrentStockTakeInfo.currentShelf.rawValue)
        def.set(nil, forKey: CurrentStockTakeInfo.currentUser.rawValue)
        def.set(nil, forKey: CurrentStockTakeInfo.currentIsQtyOne.rawValue)
        
        def.set(nil, forKey: ExportFileFormatSetup.HeaderSetting.rawValue)
        def.set(nil, forKey: ExportFileFormatSetup.Delimiter.rawValue)
        
        def.synchronize()
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        if self.isClearAll == true {
            self.isClearAll = false
            self.lblClearDateBy.isHidden = false
            self.btnCriteria.isHidden = false
        } else {
            self.isClearAll = true
            self.lblClearDateBy.isHidden = true
            self.btnCriteria.isHidden = true
        }
    }
}
