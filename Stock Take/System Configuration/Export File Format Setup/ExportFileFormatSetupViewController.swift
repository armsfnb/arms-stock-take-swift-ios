//
//  ExportFileFormatSetupViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 23/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// MARK: EXPORT FILE FORMAT SETUP
class ExportFileFormatSetupViewController: UITableViewController, UITextFieldDelegate {
    
    // MARK: Outlets
    enum fieldName: String {
        case code = "Code"
        case quantity = "Quantity"
        case branch = "Branch"
        case date = "Date"
        case location = "Location"
        case shelf = "Shelf"
    }
    
    let TAG_CODE = 1
    let TAG_QUANTITY = 2
    let TAG_BRANCH = 3
    let TAG_DATE = 4
    let TAG_LOCATION = 5
    let TAG_SHELF = 6
    
    @IBOutlet weak var viewHeaderPreview: UITextView!
    @IBOutlet weak var barBtnSave: UIBarButtonItem!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnCode: UIButton!
    @IBOutlet weak var btnQuantity: UIButton!
    @IBOutlet weak var btnBranch: UIButton!
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnShelf: UIButton!
    @IBOutlet weak var segmentDelimiter: UISegmentedControl!
    
    var delimiter: Character?
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let def = UserDefaults.standard
        
        let savedHeaderPreview = def.value(forKey: ExportFileFormatSetup.HeaderSetting.rawValue) as? String
        let savedDelimiter = def.value(forKey: ExportFileFormatSetup.Delimiter.rawValue) as? String
        
        var columnList = [String]()
        
        if savedHeaderPreview == nil || savedDelimiter == nil {
            self.delimiter = ","
            self.viewHeaderPreview.text = fieldName.code.rawValue + String(self.delimiter!) + fieldName.quantity.rawValue
            
            activateButton(for: self.btnCode, to: false)
            activateButton(for: self.btnQuantity, to: false)
        } else {
            self.delimiter = Character(savedDelimiter!)
            self.viewHeaderPreview.text = savedHeaderPreview
            
            columnList = self.viewHeaderPreview.text.components(separatedBy: String(self.delimiter!))
            
            if columnList.contains(fieldName.code.rawValue) {
                activateButton(for: self.btnCode, to: false)
            } else {
                activateButton(for: self.btnCode, to: true)
            }
            
            if columnList.contains(fieldName.quantity.rawValue) {
                activateButton(for: self.btnQuantity, to: false)
            } else {
                activateButton(for: self.btnQuantity, to: true)
            }
            
            if columnList.contains(fieldName.branch.rawValue) {
                activateButton(for: self.btnBranch, to: false)
            } else {
                activateButton(for: self.btnBranch, to: true)
            }
            
            if columnList.contains(fieldName.date.rawValue) {
                activateButton(for: self.btnDate, to: false)
            } else {
                activateButton(for: self.btnDate, to: true)
            }
            
            if columnList.contains(fieldName.location.rawValue) {
                activateButton(for: self.btnLocation, to: false)
            } else {
                activateButton(for: self.btnLocation, to: true)
            }
            
            if columnList.contains(fieldName.shelf.rawValue) {
                activateButton(for: self.btnShelf, to: false)
            } else {
                activateButton(for: self.btnShelf, to: true)
            }
            
            switch self.delimiter {
            case Character(",")?:
                self.segmentDelimiter.selectedSegmentIndex = 0
            case Character("|")?:
                self.segmentDelimiter.selectedSegmentIndex = 1
            case Character(";")?:
                self.segmentDelimiter.selectedSegmentIndex = 2
            default:
                self.segmentDelimiter.selectedSegmentIndex = 3
            }
        }
        activateButton(for: self.btnClear, to: true)
        self.barBtnSave.isEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onBtnClearTapped(_ sender: Any) {
        self.viewHeaderPreview.text = ""
        
        activateButton(for: self.btnCode, to: true)
        activateButton(for: self.btnQuantity, to: true)
        activateButton(for: self.btnBranch, to: true)
        activateButton(for: self.btnDate, to: true)
        activateButton(for: self.btnLocation, to: true)
        activateButton(for: self.btnShelf, to: true)

        activateButton(for: self.btnClear, to: false)
        self.barBtnSave.isEnabled = false
        
        self.delimiter = Character(",")
        self.segmentDelimiter.selectedSegmentIndex = 0
    }
    
    @IBAction func onBtnCodeTapped(_ sender: UIButton) {
        buttonTapped(for: sender)
    }
    
    @IBAction func onBtnQuantityTapped(_ sender: UIButton) {
        buttonTapped(for: sender)
    }
    
    @IBAction func onBtnBranchTapped(_ sender: UIButton) {
        buttonTapped(for: sender)
    }
    
    @IBAction func onBtnDateTapped(_ sender: UIButton) {
        buttonTapped(for: sender)
    }
    
    @IBAction func onBtnLocationTapped(_ sender: UIButton) {
        buttonTapped(for: sender)
    }
    
    @IBAction func onBtnShelfTapped(_ sender: UIButton) {
        buttonTapped(for: sender)
    }
    
    @IBAction func onSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        // Set old delimiter, to be replaced by new delimiter
        let oldDelimiter = self.delimiter
        
        switch self.segmentDelimiter.selectedSegmentIndex {
        case 0:
            self.delimiter = ","
        case 1:
            self.delimiter = "|"
        case 2:
            self.delimiter = ";"
        default:
            self.delimiter = " "
        }
        
        // Replace header preview delimiter
        if self.viewHeaderPreview.text != "" {
            self.viewHeaderPreview.text = self.viewHeaderPreview.text.replacingOccurrences(of: String(oldDelimiter!), with: String(self.delimiter!))
        }
    }
    
    @IBAction func onBarBtnSaveTapped(_ sender: Any) {
        
        let actionSaved = UIAlertAction(title: "OK", style: .default, handler: { action in
            let def = UserDefaults.standard
            
//            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: exportSetting)
            
            def.set(String(self.delimiter!), forKey: ExportFileFormatSetup.Delimiter.rawValue)
            def.set(self.viewHeaderPreview.text, forKey: ExportFileFormatSetup.HeaderSetting.rawValue)
            
            def.synchronize()
            
            self.navigationController?.popViewController(animated: true)
        })
        let alertController = UIAlertController(title: "Saved", message: "The export file format is saved.", preferredStyle: .alert)
        alertController.addAction(actionSaved)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Calling function when Header field is tapped
    func buttonTapped(for sender: UIButton) {
        activateButton(for: sender, to: false)
        
        if self.btnClear.isEnabled == false {
            activateButton(for: self.btnClear, to: true)
        }
        
        // Code and Quantity must be selected
        if self.btnCode.isEnabled == false && self.btnQuantity.isEnabled == false {
            self.barBtnSave.isEnabled = true
        }
        
        let rawValue: String
        
        switch sender.tag {
        case TAG_CODE:
            rawValue = fieldName.code.rawValue
        case TAG_QUANTITY:
            rawValue = fieldName.quantity.rawValue
        case TAG_BRANCH:
            rawValue = fieldName.branch.rawValue
        case TAG_DATE:
            rawValue = fieldName.date.rawValue
        case TAG_LOCATION:
            rawValue = fieldName.location.rawValue
        case TAG_SHELF:
            rawValue = fieldName.shelf.rawValue
        default:
            rawValue = ""
        }
        
        if self.viewHeaderPreview.text == "" {
            self.viewHeaderPreview.text = rawValue
        } else {
            self.viewHeaderPreview.text = self.viewHeaderPreview.text + String(self.delimiter!) + rawValue
        }
    }
}
