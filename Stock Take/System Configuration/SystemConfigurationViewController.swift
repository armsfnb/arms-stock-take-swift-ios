//
//  SystemConfigurationViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 18/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

// MARK: Stock Take Operation by Batch
class SystemConfigurationTableViewController: UITableViewController {
    
    // MARK: Outlets
    
    let SETTING_EXPORT = 1
    let SETTING_CLEAR = 2
    
    var hasStockTakeRecord: Bool = false
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        // Action: Feature Coming Soon
//        let actionComingSoon = UIAlertAction(title: "Close", style: .default, handler: nil)
        // Action: No Stock Take Record
        let actionNoRecord = UIAlertAction(title: "Close", style: .default, handler: { action in
            self.navigationController?.popViewController(animated: true)
        })

//        // Alert 1: Feature Unavailable
//        let acFeatureNotAvailable = UIAlertController(title: "Coming Soon", message: "This feature will be available soon." , preferredStyle: .alert)
//        acFeatureNotAvailable.addAction(actionComingSoon)

        // Alert 2: No Stock Take Record
        let acNoRecord = UIAlertController(title: "No Stock Take Record", message: "Database is now empty." , preferredStyle: .alert)
        acNoRecord.addAction(actionNoRecord)

        // Check if there are stock take records
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        do {
            let countStockTake = try context.count(for:StockTakeRecord.fetchRequest())
            self.hasStockTakeRecord = (countStockTake != 0) ? true : false
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
        if indexPath.row == SETTING_CLEAR && self.hasStockTakeRecord == false {
            present(acNoRecord, animated: true, completion: nil)
        }
        
//        if indexPath.row == SETTING_IMPORT {
//            present(acFeatureNotAvailable, animated: true, completion: nil)
//        } else {
//            if indexPath.row == SETTING_CLEAR && self.hasStockTakeRecord == false {
//                present(acNoRecord, animated: true, completion: nil)
//            }
//        }
    }
    
    @IBAction func onBarBtnCancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
