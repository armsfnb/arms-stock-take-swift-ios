//
//  StockTakeSystemTableViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 03/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import CoreData

// MARK: Stock Take Operation by Batch
class StockTakeSystemTableViewController: UITableViewController, UIDocumentPickerDelegate {
    
    // MARK: Outlets
    var hasStockTakeRecord: Bool = false
    
    let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    
    // Stock Take Main Menu
    let STOCK_TAKE = 1
    let PRICE_CHECK = 2
    let REPORT_LISTING = 3
    let IMPORT_EXPORT = 4
    let UPGRADE_ACTIVATION = 5
    
    let TOTAL_COLUMN = 26
    let IMPORT_DATE = "importDate"
    
    // Import SKU Data & Price Check
    enum colName: Int {
        case skuItemID = 0, armsCode, artno, mcode, barcode, productDesc, receiptDesc, sellingPrice, costPrice, priceType, skuType, department, category, skuOrPrc, uomCode, fraction, inputTax, outputTax, inclusiveTax, scaleType, active, brand, vendor, parentArmsCode, parentArtno, parentMcode
    }
    
    lazy var recordList: [String]? = {
        let item = [String]()
        return item
    }()
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Alert 1: Feature Coming Soon
        let actionComingSoon = UIAlertAction(title: "Close", style: .default, handler: nil)
        let acFeatureNotAvailable = UIAlertController(title: "Coming Soon", message: "This feature will be available soon." , preferredStyle: .alert)
        acFeatureNotAvailable.addAction(actionComingSoon)
        
        // Alert 2: No Stock Take Record
        let actionNoRecord = UIAlertAction(title: "Close", style: .default, handler: { action in
            self.navigationController?.popViewController(animated: true)
        })
        let acNoRecord = UIAlertController(title: "No Stock Take Record", message: "Please create new Stock Take Record before viewing report." , preferredStyle: .alert)
        acNoRecord.addAction(actionNoRecord)
        
        // Check if there is stock take records
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
//        context.concurrencyType = .privateQueueConcurrencyType
        do {
            let countStockTake = try context.count(for:StockTakeRecord.fetchRequest())
            self.hasStockTakeRecord = (countStockTake != 0) ? true : false
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
        // Report Listing
        if indexPath.row == REPORT_LISTING && self.hasStockTakeRecord == false {
            present(acNoRecord, animated: true, completion: nil)
        }
        
        // Import SKU Data
        if indexPath.row == IMPORT_EXPORT {
            showDocumentPicker()
        }
        
        // Upgrade and activation
        if indexPath.row > IMPORT_EXPORT {
            present(acFeatureNotAvailable, animated: true, completion: nil)
        }
    }
    
    // MARK: Document Picker
    // Import SKU Data & Price Check
    func showDocumentPicker(){
        let docPicker = UIDocumentPickerViewController(documentTypes: ["public.text"], in: UIDocumentPickerMode.import)
        docPicker.delegate = self
        docPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(docPicker, animated: true, completion: nil)
    }

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {

        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as URL
        let destinationFileUrl = documentsUrl.appendingPathComponent(url.pathComponents.last!)

        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)

        let request = URLRequest(url: url)

        // Download selected file
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalURL = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                
                do {
                    // Remove file with the same path
                    if (FileManager.default.fileExists(atPath: destinationFileUrl.path)) {
                        try FileManager.default.removeItem(at: destinationFileUrl)
                    }

                    // Make new file copy from tmp directory to Document directory
                    try FileManager.default.copyItem(at: tempLocalURL, to: destinationFileUrl)
                    
                    // Warning: UIApplication.delegate must be used from main thread only
                    // read text from file in Document directory
                    if let _ = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                        do {
                            // throws error if found abnormal characters in file when exported from Backend module
                            let text = try String(contentsOf: destinationFileUrl, encoding: .ascii)
                            let newString = text.components(separatedBy: .newlines)
//                            let totalColumn = 26  // hard-coded
                            let columnNumber = newString[0].components(separatedBy: ",").count
//                            print(myStrings.first!)
//                            print(myStrings[3])

//                            if columnNumber == totalColumn {
                            if columnNumber == self.TOTAL_COLUMN {
                                // call import function.
//                                self.importSKUData(from: newString)
                                self.recordList = newString
                                self.importSKUData(from: self.recordList!)

                            } else {
                                // Action: Invalid file
                                let action = UIAlertAction(title: "Close", style: .default, handler: nil)
                                let actionController = UIAlertController(title: "Error", message: "Invalid file selected. Please ensure the selected file contains SKU data exported from Backend module." , preferredStyle: .alert)

                                actionController.addAction(action)
                                self.present(actionController, animated: true, completion: nil)
                            }
                        }
                        catch {
                            print("Cannot read text from .csv file. The file contains invalid characters.")
                        }
                    }
                    
                    
                }
                catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
            } else {
                print("Error took place while downloading a file.")
            }
        }

        task.resume()
    }

    // Save into entity: SKUItem
    func importSKUData(from list: [String]) {
        // Alert: Import started (automatically dismiss when import has finished)
        let acImportStart = UIAlertController(title: "Import in Progress", message: "This action might take a few minutes.", preferredStyle: .alert)
        present(acImportStart, animated: true, completion: nil)
        
        
        // Progress bar
//            present(acImportStart, animated: true, completion: {
//            //  Add your progressbar after alert is shown (and measured)
//            let margin: CGFloat = 8.0
////            let rect = CGRect(margin, 72.0, acImportStart.view.frame.width - margin * 2.0 , 2.0)
//            let rect = CGRect(x: margin, y: 72.0, width: acImportStart.view.frame.width - margin * 2.0, height: 2.0)
//            let progressView = UIProgressView(frame: rect)
//
//
////            progressView.progress = 0.5   // this one!
//
//            progressView.setProgress(0.01, animated: true)
////            UIView.animate(withDuration: 5, delay: 0.0, options: .curveLinear, animations: { progressView.setProgress(1.0, animated: true) } , completion: { (Bool) -> Void in
////                progressView.progress = 0.0
////            })
//
//            UIView.animate(withDuration: 5, animations: { () -> Void in
//                progressView.setProgress(1, animated: true)
//            }, completion: { (Bool) -> Void in
//                if progressView.progress == 1.0 {
//                    progressView.setProgress(0.0, animated: true)
//
//                }
//            })
//
//            progressView.tintColor = UIColor.blue
//            acImportStart.view.addSubview(progressView)
//        })
        
        // Remove all data before importing new data
        let context = appDelegate.persistentContainer.newBackgroundContext()
        
        let fetchSKUItem: NSFetchRequest<SKUItem> = SKUItem.fetchRequest()
        var SKUItems: [SKUItem]?
        do {
            SKUItems = try context.fetch(fetchSKUItem as! NSFetchRequest<NSFetchRequestResult>) as? [SKUItem]
            for item in SKUItems! {
                let objectData: NSManagedObject = item as NSManagedObject
                context.delete(objectData)
            }
        } catch {
            fatalError("Failed to fetch branch: \(error)")
        }
        
        var index = 0
        var skuCount = 0
        
        // new array without empty last row
        var tempList = list
        let _ = tempList.removeLast()
        
        var joinedDetails: [String]?
        var errorList: [String] = []
        
        for record in tempList {
            if index > 0 {
                var recordDetails = record.components(separatedBy: ",")
                
                // line which has artno/product description/etc containing comma(s)
                if recordDetails.count > TOTAL_COLUMN {
                    print("------\(recordDetails[0])")
                    errorList.append(recordDetails.joined(separator: ","))
                    continue
                }
                
                // line separated by newline character (Chinese character)
                if recordDetails.count < TOTAL_COLUMN {
                    if let _ = joinedDetails {
                        // join with next line
                        let lastElement = joinedDetails?.last
                        let firstElement = recordDetails.first
                        
                        let _ = joinedDetails?.removeLast()
                        joinedDetails?.append(lastElement! + firstElement!)
                        
                        let _ = recordDetails.removeFirst()
                        joinedDetails = joinedDetails! + recordDetails
                        
                    } else {
                        // initiate new line to be joined
                        joinedDetails = recordDetails
                    }
                }
                
                if joinedDetails?.count == TOTAL_COLUMN {
                    recordDetails = joinedDetails!
                    joinedDetails = nil
                    print("joined")
                } else if let _ = joinedDetails {
                    print("skipped")
                    print(joinedDetails!.count)
                    if joinedDetails!.count > TOTAL_COLUMN {
                        print("------\(joinedDetails![0])")
                        errorList.append(joinedDetails!.joined(separator: ","))
                        joinedDetails = nil
                    }
                    continue
                }
                
                let savedSKUItem = SKUItem(context: context)
                
                // Int16 causes overflow
                // 0 - 4
                savedSKUItem.skuItemID = Int32(recordDetails[colName.skuItemID.rawValue].replacingOccurrences(of: "\"", with: ""))!
                savedSKUItem.armsCode = recordDetails[colName.armsCode.rawValue].replacingOccurrences(of: "\"", with: "")
                savedSKUItem.artno = recordDetails[colName.artno.rawValue]
                savedSKUItem.mcode = recordDetails[colName.mcode.rawValue]
                savedSKUItem.barcode = recordDetails[colName.barcode.rawValue]
                
                // 5 - 9
                let prodDescStr = recordDetails[colName.productDesc.rawValue]
                let prodDescStrSep = prodDescStr.components(separatedBy: "\"")
                var prodDescStrList: [String] = []
                for str in prodDescStrSep {
                    if str != "" {
                        prodDescStrList.append(str)
                    }
                }
                if prodDescStrList.count == 1 {
                    savedSKUItem.productDescription = prodDescStrList[0]
                } else {
                    savedSKUItem.productDescription = "\"" + prodDescStrList[0]
                    for index in 1..<prodDescStrList.count {
                        savedSKUItem.productDescription?.append("\"")
                        savedSKUItem.productDescription?.append(prodDescStrList[index])
                    }
                }
                savedSKUItem.receiptDescription = recordDetails[colName.receiptDesc.rawValue]
                savedSKUItem.sellingPrice = Double(recordDetails[colName.sellingPrice.rawValue].replacingOccurrences(of: "\"", with: ""))!
                savedSKUItem.costPrice = Double(recordDetails[colName.costPrice.rawValue].replacingOccurrences(of: "\"", with: ""))!
                savedSKUItem.priceType = recordDetails[colName.priceType.rawValue]
                
                // 10 - 14
                savedSKUItem.skuType = recordDetails[colName.skuType.rawValue]
                savedSKUItem.department = recordDetails[colName.department.rawValue].replacingOccurrences(of: "\"", with: "")
                savedSKUItem.category = recordDetails[colName.category.rawValue].replacingOccurrences(of: "\"", with: "")
                savedSKUItem.skuOrPrc = recordDetails[colName.skuOrPrc.rawValue]
                savedSKUItem.uomCode = recordDetails[colName.uomCode.rawValue]
                
                //15 - 19
                savedSKUItem.fraction = Int16(recordDetails[colName.fraction.rawValue])!
                savedSKUItem.inputTax = recordDetails[colName.inputTax.rawValue]
                savedSKUItem.outputTax = recordDetails[colName.outputTax.rawValue]
                if recordDetails[colName.inclusiveTax.rawValue].range(of: "YES") != nil {
                    savedSKUItem.inclusiveTax = true
                } else {
                    savedSKUItem.inclusiveTax = false
                }
                savedSKUItem.scaleType = recordDetails[colName.scaleType.rawValue].replacingOccurrences(of: "\"", with: "")
                
                // 20-24
                if recordDetails[colName.active.rawValue].range(of: "YES") != nil {
                    savedSKUItem.active = true
                } else {
                    savedSKUItem.active = false
                }
                savedSKUItem.brand = recordDetails[colName.brand.rawValue].replacingOccurrences(of: "\"", with: "")
                savedSKUItem.vendor = recordDetails[colName.vendor.rawValue].replacingOccurrences(of: "\"", with: "")
                savedSKUItem.parentArmsCode = recordDetails[colName.parentArmsCode.rawValue].replacingOccurrences(of: "\"", with: "")
                savedSKUItem.parentArtno = recordDetails[colName.parentArtno.rawValue]
                
                // 25
                savedSKUItem.parentMcode = recordDetails[colName.parentMcode.rawValue]
                
                do {
                    try context.save()
                    print("Saved")
                    skuCount += 1
                } catch {
                    print("Error saving SKU items.")
                }
            }
            index += 1
        }
        self.commit()
        
        print(errorList.count)
        
        // Set current date as Import Date
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        let importDate = df.string(from: Date.init())
        
        // Import Date is saved and shown in Price Check module
        let def = UserDefaults.standard
        def.set(importDate, forKey: IMPORT_DATE)
        def.synchronize()
        
        acImportStart.dismiss(animated: true, completion: nil)
        
        // Alert: Import finished
        let actionImportFinish = UIAlertAction(title: "Close", style: .default, handler: nil)
        let acImportFinish = UIAlertController(title: "Import Successful", message: "Database imported on: " + importDate + "\nNo of SKU records: " + String(skuCount), preferredStyle: .alert)
        acImportFinish.addAction(actionImportFinish)
        present(acImportFinish, animated: true, completion: nil)
    }
}
