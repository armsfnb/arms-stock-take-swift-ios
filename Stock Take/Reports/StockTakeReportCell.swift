//
//  StockTakeReportCell.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 13/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class StockTakeReportCell: UITableViewCell {
    
    @IBOutlet weak var itemCode: UILabel!
    @IBOutlet weak var itemQty: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
