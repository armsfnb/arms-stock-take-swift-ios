//
//  SelectCriteriaTableViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 13/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//protocol SelectCriteriaTableViewControllerDelegate{
//    func didSelectBatchNo(_ sender: UIDatePicker, selectedBatchNo: String)
//}

// MARK: Select Criteria View Controller
class SelectCriteriaTableViewController: UITableViewController {
    
    // MARK: Outlets
    @IBOutlet weak var navTitle: UINavigationItem!
    
    var delegate: GenerateReportViewController?
    var selectedCriteria: criteriaType?
    var selectedElement: String?
    
    lazy var elementList: [String]? = {
        let element = [String]()
        return element
    }()
    
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadElementList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if let elementList = self.elementList {
            count = elementList.count
        }
        return count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if let element = self.elementList?[indexPath.row] {
            cell.textLabel?.text = element
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let element = self.elementList?[indexPath.row] {
            self.selectedElement = element
            switch self.selectedCriteria {
            case .branch?:
                self.delegate?.selectedBranch = element
                self.delegate?.txtSelectedBranch.text = element
            case .location?:
                self.delegate?.selectedLocation = element
                self.delegate?.txtSelectedLocation.text = element
            case .shelf?:
                self.delegate?.selectedShelf = element
                self.delegate?.txtSelectedShelf.text = element
            case .user?:
                self.delegate?.selectedUser = element
                self.delegate?.txtSelectedUser.text = element
            default:
                break
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func barBtnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func barBtnAll(_ sender: Any) {
        switch self.selectedCriteria {
        case .branch?:
            self.delegate?.selectedBranch = SELECT_ALL
            self.delegate?.txtSelectedBranch.text = SELECT_ALL
        case .location?:
            self.delegate?.selectedLocation = SELECT_ALL
            self.delegate?.txtSelectedLocation.text = SELECT_ALL
        case .shelf?:
            self.delegate?.selectedShelf = SELECT_ALL
            self.delegate?.txtSelectedShelf.text = SELECT_ALL
        case .user?:
            self.delegate?.selectedUser = SELECT_ALL
            self.delegate?.txtSelectedUser.text = SELECT_ALL
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadElementList() {
        let def = UserDefaults.standard
        
        let branchList = def.stringArray(forKey: StockTakeInfoList.branchList.rawValue) ?? [String]()
        let locationList = def.stringArray(forKey: StockTakeInfoList.locationList.rawValue) ?? [String]()
        let shelfList = def.stringArray(forKey: StockTakeInfoList.shelfList.rawValue) ?? [String]()
        let userList = def.stringArray(forKey: StockTakeInfoList.userList.rawValue) ?? [String]()
        
        switch self.selectedCriteria {
        case .branch?:
            self.elementList = branchList
        case .location?:
            self.elementList = locationList
        case .shelf?:
            self.elementList = shelfList
        case .user?:
            self.elementList = userList
        default:
            break
        }
        
        self.elementList?.sort()
        
//        let fetchStockTakeRequests: NSFetchRequest<Stock_take> = Stock_take.fetchRequest()
//        do {
//            let stockTakes = try self.appContext().fetch(fetchStockTakeRequests)
//            if stockTakes.count > 0 {
//                for record in stockTakes {
//                    switch self.selectedCriteria {
//                    case .branch?:
//                        if self.elementList?.contains(record.to_branch!.branch_name!) == false {
//                            self.elementList?.append(record.to_branch!.branch_name!)
//                        }
//                    case .location?:
//                        if self.elementList?.contains(record.to_location!.location_name!) == false {
//                            self.elementList?.append(record.to_location!.location_name!)
//                        }
//                    case .shelf?:
//                        if self.elementList?.contains(record.to_shelf!.shelf_name!) == false {
//                            self.elementList?.append(record.to_shelf!.shelf_name!)
//                        }
//                    case .user?:
//                        if self.elementList?.contains(record.to_user!.username!) == false {
//                            self.elementList?.append(record.to_user!.username!)
//                        }
//                    default:
//                        break
//                    }
//                }
//                self.elementList?.sort()
//            }
//        } catch let error {
//            print(error.localizedDescription)
//        }

    }
}

