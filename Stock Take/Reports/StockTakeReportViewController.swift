//
//  StockTakeReportViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 13/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import MessageUI
import Zip

class StockTakeReportViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    // MARK: Outlets
    // Export file format setup
    enum fieldName: String {
        case code = "Code"
        case quantity = "Quantity"
        case branch = "Branch"
        case date = "Date"
        case location = "Location"
        case shelf = "Shelf"
    }
    
    @IBOutlet weak var barBtnPrint: UIBarButtonItem!
    
    @IBOutlet weak var stockTakeTable: UITableView!
    
    var selectedBranch: String?
    var selectedDateString: String?
    var selectedLocation: String?
    var selectedShelf: String?
    var selectedUser: String?
    
    var dfSelected = DateFormatter()
    
    var hasStockTakeRecord: Bool?
    
    lazy var stockTakeItemList: [StockTakeRecord]? = {
        let item = [StockTakeRecord]()
        return item
    }()
    
    var selectedItem: StockTakeRecord?
    var identifier = "showStockTakeRecordDetails"
    
    // Setup Export file format
    var exportHeader: String?
    var delimiter: Character?
    
    lazy var fieldList: [String]? = {
        let item = [String]()
        return item
    }()
    
    var stockTakeGroupByLocShelf: [String:[StockTakeRecord]] = [:] // key: "<location> → <shelf>"; value: array of Stock Take Records
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        // hide bar button Print
        self.barBtnPrint.tintColor = UIColor.clear
        fetchStockTakeItemRecord()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.stockTakeTable.reloadData()
        
        let countStockTake = self.stockTakeGroupByLocShelf.count
        self.hasStockTakeRecord = (countStockTake != 0) ? true : false
        
        if self.hasStockTakeRecord == false {
            let action = UIAlertAction(title: "Close", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            })
            let alertController = UIAlertController(title: "Error", message: "No record found." , preferredStyle: .alert)
            alertController.addAction(action)
            
            present(alertController, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.identifier {
            if let vc = segue.destination as? StockTakeDetailsViewController {
                if let indexPath = self.stockTakeTable.indexPathForSelectedRow {
                    var keyStockTake = Array(self.stockTakeGroupByLocShelf.keys)
                    keyStockTake = keyStockTake.sorted()
                    
                    let itemList = self.stockTakeGroupByLocShelf[keyStockTake[indexPath.section]]
                    
                    vc.selectedItem = itemList?[indexPath.row]
                    vc.indexPath = [indexPath]
                    vc.selectedSection = keyStockTake[indexPath.section]
                    vc.delegate = self
                }
            }
        }
    }
    
    @IBAction func onBarBtnExportTapped(_ sender: Any) {
        let df = DateFormatter()
        df.dateFormat = "ddMMyyyy"
        
        var fileNameList = [String]()
        var pathList = [URL?]()
        
        let keyStockTake = Array(self.stockTakeGroupByLocShelf.keys)
        
        // Load saved Export File Format
        let def = UserDefaults.standard
        self.exportHeader = def.value(forKey: ExportFileFormatSetup.HeaderSetting.rawValue) as? String
//        self.delimiter = Character(def.string(forKey: ExportFileFormatSetup.Delimiter.rawValue)!)
        
        self.delimiter = def.string(forKey: ExportFileFormatSetup.Delimiter.rawValue) != nil ? Character(def.string(forKey: ExportFileFormatSetup.Delimiter.rawValue)!) : nil
        
        var csvText: String
        if self.exportHeader == nil || self.delimiter == nil {
            // No saved Export File Format
            csvText = "Code,Quantity\n"
        } else {
            // Saved Export File Format
            csvText = "\(self.exportHeader!)\n"
            self.fieldList = (self.exportHeader?.components(separatedBy: String(self.delimiter!)))!
        }
        var csvTextList: [String] = []
        var itemDetail: String
        
        var index = 0
        for key in keyStockTake {
            let keyReplaced = key.replacingOccurrences(of: " → ", with: "_")
            let fileName = "ST_\(df.string(from: Date()))_\(keyReplaced).csv" // .csv file
            fileNameList.append(fileName)
            pathList.append(NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName))
            
            csvTextList.append(csvText)
            
            for item in self.stockTakeGroupByLocShelf[key]! {
                
                var newLine: String = ""
                
                if self.exportHeader == nil || self.delimiter == nil {
                    // No saved Export File Format
                    newLine = "\(item.itemCode!),\(String(item.itemQty))\n"
                } else {
                    // Saved Export File Format
                    // Setup New Line details
                    for field in self.fieldList! {
                        switch field {
                        case fieldName.code.rawValue:
                            itemDetail = item.itemCode!
                        case fieldName.quantity.rawValue:
                            itemDetail = String(item.itemQty)
                        case fieldName.branch.rawValue:
                            itemDetail = item.branch!
                        case fieldName.date.rawValue:
                            let df = DateFormatter()
                            df.dateFormat = "dd/MM/yyyy"
                            itemDetail = df.string(from: item.dateStockTake!)
                        case fieldName.location.rawValue:
                            itemDetail = item.location!
                        case fieldName.shelf.rawValue:
                            itemDetail = item.shelf!
                        default:
                            itemDetail = ""
                        }
                        
                        if newLine == "" {
                            newLine = itemDetail
                        } else {
                            newLine = newLine + String(self.delimiter!) + itemDetail
                        }
                    }
                    newLine = newLine + "\n"
                }
                // Append New Line to the list
                csvTextList[index].append(contentsOf: newLine)
            }
            index += 1
        }
        
        do {
            for index in 0..<keyStockTake.count {
                try csvTextList[index].write(to: pathList[index]!, atomically: true, encoding: String.Encoding.utf8)
            }
            
            if MFMailComposeViewController.canSendMail() {
                let emailController = MFMailComposeViewController()
                emailController.mailComposeDelegate = self
                emailController.setSubject("ST_\(df.string(from: Date()))")
                
                do {
                    // Zip all files from pathList
                    let zipFilePath = try Zip.quickZipFiles(pathList as! [URL], fileName: "ST_\(df.string(from: Date()))")
                    guard let optData = try? Data(contentsOf: zipFilePath) else {
                        return
                    }
                    // Send mail as zip file
                    emailController.addAttachmentData(optData, mimeType: "application/zip", fileName: "ST_\(df.string(from: Date()))")
                }
                catch {
                    print("Something went wrong")
                }

                self.present(emailController, animated: true, completion: nil)
            }
            else {
                let actionSetMail = UIAlertAction(title: "Close", style: .default, handler: nil)
                let alertSetMail = UIAlertController(title: "Error", message: "Please set up device email to export report.", preferredStyle: .alert)
                alertSetMail.addAction(actionSetMail)
                
                self.present(alertSetMail, animated: true, completion: nil)
            }
        } catch {
            print("Failed to create file")
            print("\(error)")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func fetchStockTakeItemRecord() {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchStockTakeRecord: NSFetchRequest<StockTakeRecord> = StockTakeRecord.fetchRequest()
        var stockTakeRecords: [StockTakeRecord]?
        do {
            stockTakeRecords = try context.fetch(fetchStockTakeRecord as! NSFetchRequest<NSFetchRequestResult>) as? [StockTakeRecord]
        } catch {
            fatalError("Failed to fetch Stock Take Records: \(error)")
        }
        
        self.stockTakeItemList?.removeAll()
        
        for item in stockTakeRecords! {
            if (self.selectedBranch == SELECT_ALL || item.branch == self.selectedBranch) &&
                (self.selectedDateString == SELECT_ALL || dfSelected.string(from: (item.dateStockTake!)) == self.selectedDateString) &&
                (self.selectedLocation == SELECT_ALL || item.location == self.selectedLocation) &&
                (self.selectedShelf == SELECT_ALL || item.shelf == self.selectedShelf) &&
                (self.selectedUser == SELECT_ALL || item.user == self.selectedUser)
            {
                self.stockTakeItemList?.append(item)
            }
        }
        // StockTakeGroupByLocShelf
        for item in stockTakeItemList! {
            let itemLocShelfKey = item.location! + " → " + item.shelf!
            let keyStockTake = Array(self.stockTakeGroupByLocShelf.keys)
            
            if keyStockTake.contains(itemLocShelfKey) {
                self.stockTakeGroupByLocShelf[itemLocShelfKey]!.append(item)
            } else {
                self.stockTakeGroupByLocShelf[itemLocShelfKey] = [item]
            }
        }
    }
}

// MARK: STOCK TAKE REPORT CELL
extension StockTakeReportViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.stockTakeGroupByLocShelf.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var keyStockTake = Array(self.stockTakeGroupByLocShelf.keys)
        keyStockTake = keyStockTake.sorted()
        
        if self.stockTakeGroupByLocShelf[keyStockTake[section]]!.count > 0 {
            return keyStockTake[section]
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var keyStockTake = Array(self.stockTakeGroupByLocShelf.keys)
        keyStockTake = keyStockTake.sorted()
        
        return self.stockTakeGroupByLocShelf[keyStockTake[section]]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StockTakeReportCell", for: indexPath) as! StockTakeReportCell
        
        var keyStockTake = Array(self.stockTakeGroupByLocShelf.keys)
        keyStockTake = keyStockTake.sorted()
        
        let itemList = self.stockTakeGroupByLocShelf[keyStockTake[indexPath.section]]
        
        if let item = itemList?[indexPath.row] {
            cell.itemCode.text = item.itemCode
            cell.itemQty.text = String(item.itemQty)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: self.identifier, sender: indexPath)
    }
}
