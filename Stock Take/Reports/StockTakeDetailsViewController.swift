
//
//  StockTakeDetailsViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 17/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class StockTakeDetailsViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var lblDetailStockID: UILabel!
    @IBOutlet weak var txtDetailStockQty: UITextField!
    @IBOutlet weak var lblDetailBranch: UILabel!
    @IBOutlet weak var lblDetailDate: UILabel!
    @IBOutlet weak var lblDetailLocation: UILabel!
    @IBOutlet weak var lblDetailShelf: UILabel!
    @IBOutlet weak var lblDetailUser: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    var selectedItem: StockTakeRecord?
    var selectedSection: String?
    var qtyInt: Int?
    var indexPath: [IndexPath]?
    
    var delegate: StockTakeReportViewController?
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtDetailStockQty.delegate = self
        
        self.lblDetailStockID.text = self.selectedItem?.itemCode
        self.txtDetailStockQty.text = String(describing: (self.selectedItem?.itemQty)!)
        self.lblDetailBranch.text = self.selectedItem?.branch
        
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        self.lblDetailDate.text = df.string(from: (self.selectedItem?.dateStockTake!)!)
        
        self.lblDetailLocation.text = self.selectedItem?.location
        self.lblDetailShelf.text = self.selectedItem?.shelf
        self.lblDetailUser.text = self.selectedItem?.user
        
        self.qtyInt = Int(self.txtDetailStockQty.text!)
        
        activateButton(for: self.btnReset, to: false)
        activateButton(for: self.btnSave, to: false)
        
        // Tap anywhere to dismiss the keyboard.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //--- add UIToolBar on keyboard and Done button on UIToolBar ---//
        self.addDoneButtonOnKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let intStockTake = Int(self.txtDetailStockQty.text!), intStockTake > 0 {
            self.qtyInt = intStockTake
            self.stepper.value = Double(intStockTake)
            self.txtDetailStockQty.text = String(intStockTake)
        } else {
            self.qtyInt = 1
            self.stepper.value = 0.0
            self.txtDetailStockQty.text = "1"
        }
        
        if (self.btnReset.isEnabled == false && self.btnSave.isEnabled == false) {
            activateButton(for: self.btnReset, to: true)
            activateButton(for: self.btnSave, to: true)
        }
    }
    
    func resetStepper(stepper: UIStepper) {
        stepper.value = 0.0
    }
    
    func incrementCount(stepper: UIStepper) {
        let newValue = totalCount() + Int(stepper.value)
        
        if newValue > 0 {
            self.txtDetailStockQty.text = "\(newValue)"
        } else {
            self.txtDetailStockQty.text = "1"
        }
        
        if (self.btnReset.isEnabled == false && self.btnSave.isEnabled == false) {
            activateButton(for: self.btnReset, to: true)
            activateButton(for: self.btnSave, to: true)
        }
        resetStepper(stepper: stepper)
    }
    
    func totalCount() -> Int {
        return Int(self.txtDetailStockQty.text!)!
    }
    
    @IBAction func onStepperValueChanged(_ sender: UIStepper) {
        incrementCount(stepper: sender)
        qtyInt = Int(self.txtDetailStockQty.text!)
    }
    
    @IBAction func onResetButtonTapped(_ sender: Any) {
        self.viewDidLoad()
    }
    
    @IBAction func onSaveButtonTapped(_ sender: Any) {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchStockTakeRecord: NSFetchRequest<StockTakeRecord> = StockTakeRecord.fetchRequest()
        var stockTakeRecords: [StockTakeRecord]?
        do {
            stockTakeRecords = try context.fetch(fetchStockTakeRecord as! NSFetchRequest<NSFetchRequestResult>) as? [StockTakeRecord]
            // NSManagedObjectID
            for item in stockTakeRecords! {
                let objectData: NSManagedObject = item as NSManagedObject
                if self.selectedItem?.dateSaved == item.dateSaved {
                    objectData.setValue(Int64(self.qtyInt!), forKey: "itemQty")
                    objectData.setValue(Date(), forKey: "dateSaved")
                    do {
                        try context.save()
                        print("Saved!")
                    } catch {
                        fatalError("Failed to update Stock Take Item: \(error)")
                    }
                    break
                }
            }
        } catch {
            fatalError("Failed to fetch Stock Take Item: \(error)")
        }
        self.commit()
        
        let action = UIAlertAction(title: "OK", style: .default, handler: { action in
            self.navigationController?.popViewController(animated: true)
        })
        let alertController = UIAlertController(title: "Saved", message: "Stock take item qty updated." , preferredStyle: .alert)
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onBarBtnDeleteTapped(_ sender: Any) {
        // Action: Delete
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { action in
            // Action: Confirm delete record
            let action = UIAlertAction(title: "OK", style: .default, handler: { action in
                let hasRecord = self.deleteRecord()
                if hasRecord {
                    self.delegate?.stockTakeTable.deleteRows(at: self.indexPath!, with: .automatic)

                    self.navigationController?.popViewController(animated: true)
                } else {
                    // Action: Back to main menu
                    let actionBackToRoot = UIAlertAction(title: "OK", style: .default, handler: {action in
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                    // Alert 3: No Stock Take Record
                    let acBackToRoot = UIAlertController(title: "No record", message: "Database is now empty. System will navigate to Main menu", preferredStyle: .alert)
                    acBackToRoot.addAction(actionBackToRoot)
                    
                    self.present(acBackToRoot, animated: true, completion: nil)
                }
            })
            // Alert 2: Record deleted
            let alertController = UIAlertController(title: "Record deleted", message: "This Stock Take record has been deleted." , preferredStyle: .alert)
            alertController.addAction(action)
            
            self.present(alertController, animated: true, completion: nil)
        })
        // Action: Cancel
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        // Alert 1: Are you sure?
        let acConfirm = UIAlertController(title: "Are you sure?", message: "This action cannot be undone.", preferredStyle: .alert)
        acConfirm.addAction(cancelAction)
        acConfirm.addAction(deleteAction)
        
        self.present(acConfirm, animated: true, completion: nil)
    }
    
    func deleteRecord() -> Bool {
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchStockTakeRecord: NSFetchRequest<StockTakeRecord> = StockTakeRecord.fetchRequest()
        var stockTakeRecords: [StockTakeRecord]?
        
        do {
            stockTakeRecords = try context.fetch(fetchStockTakeRecord as! NSFetchRequest<NSFetchRequestResult>) as? [StockTakeRecord]
            
            for item in stockTakeRecords! {
                let objectData: NSManagedObject = item as NSManagedObject
                if self.selectedItem?.dateSaved == item.dateSaved {
                    context.delete(objectData)  // remove item from database
                    
                    let filteredList = self.delegate?.stockTakeGroupByLocShelf[selectedSection!]?.filter {$0.dateSaved != item.dateSaved}
                    self.delegate?.stockTakeGroupByLocShelf[selectedSection!] = filteredList
                    
                    break
                }
            }
            
        } catch {
            fatalError("Failed to fetch Stock Take Item: \(error)")
        }
        self.commit()
        
        // update UserDefaults List
        let def = UserDefaults.standard
        
        var branchList = [String]()
        var minDate: Date?
        var maxDate: Date?
        var dateList = [String]()
        var locationList = [String]()
        var shelfList = [String]()
        var userList = [String]()
        
        do {
            stockTakeRecords = try context.fetch(fetchStockTakeRecord as! NSFetchRequest<NSFetchRequestResult>) as? [StockTakeRecord]
            for item in stockTakeRecords! {
                if branchList.contains(item.branch!) == false {
                    branchList.append(item.branch!)
                }
                
                let df = DateFormatter()
                df.dateFormat = "dd/MM/yyyy"
                if dateList.contains(df.string(from: item.dateStockTake!)) == false {
                    dateList.append(df.string(from: item.dateStockTake!))
                }
                
                if locationList.contains(item.location!) == false {
                    locationList.append(item.location!)
                }
                
                if shelfList.contains(item.shelf!) == false {
                    shelfList.append(item.shelf!)
                }
                
                if userList.contains(item.user!) == false {
                    userList.append(item.user!)
                }
                
                if let _ = minDate {
                    if minDate! > item.dateStockTake! {
                        minDate = item.dateStockTake
                    }
                } else {
                    minDate = item.dateStockTake
                }
                
                if let _ = maxDate {
                    if maxDate! < item.dateStockTake! {
                        maxDate = item.dateStockTake
                    }
                } else {
                    maxDate = item.dateStockTake
                }
            }
        } catch {
            fatalError("Failed to fetch Stock Take Item: \(error)")
        }
        
        def.set(branchList, forKey: StockTakeInfoList.branchList.rawValue)
        def.set(dateList, forKey: StockTakeInfoList.dateList.rawValue)
        def.set(locationList, forKey: StockTakeInfoList.locationList.rawValue)
        def.set(shelfList, forKey: StockTakeInfoList.shelfList.rawValue)
        def.set(userList, forKey: StockTakeInfoList.userList.rawValue)
        
        def.set(minDate, forKey: StockTakeInfoList.dateMin.rawValue)
        def.set(maxDate, forKey: StockTakeInfoList.dateMax.rawValue)
        
        def.synchronize()
        
        if (stockTakeRecords?.count)! > 0 {
            return true
        } else {
            return false
        }
    }
    
    @objc func dismissKeyboard() {
//        view.endEditing(true)
    }
    
    // Add Done button to the keyboard.
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(StockTakeOperationViewController.doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.txtDetailStockQty.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.txtDetailStockQty.resignFirstResponder()
    }
}
