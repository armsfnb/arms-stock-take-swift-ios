//
//  GenerateReportViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 13/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// MARK: STOCK TAKE OPERATION BY BATCH
class GenerateReportViewController: UITableViewController {
    
    // MARK: Outlets
    private struct Storyboard {
        static let showSelectBranch = "showSelectBranch"
        static let showSelectDate = "showSelectDate"
        static let showSelectLocation = "showSelectLocation"
        static let showSelectShelf = "showSelectShelf"
        static let showSelectUser = "showSelectUser"
        static let generateReport = "generateReport"
    }
    
    @IBOutlet weak var txtSelectedBranch: UITextField!
    @IBOutlet weak var btnSelectedDate: UIButton!
    @IBOutlet weak var txtSelectedLocation: UITextField!
    @IBOutlet weak var txtSelectedShelf: UITextField!
    @IBOutlet weak var txtSelectedUser: UITextField!
    
    var selectedBranch: String?
    var selectedDate: Date?
    var selectedLocation: String?
    var selectedShelf: String?
    var selectedUser: String?
    
    var selectedDateString: String?
    var dfSelected = DateFormatter()
    
    var hasStockTakeRecord: Bool = true
    
    // MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedBranch = SELECT_ALL
        self.selectedDateString = SELECT_ALL
        self.selectedLocation = SELECT_ALL
        self.selectedShelf = SELECT_ALL
        self.selectedUser = SELECT_ALL
        
        self.txtSelectedBranch.text = self.selectedBranch
        self.btnSelectedDate.setTitle(self.selectedDateString, for: .normal)
        self.txtSelectedLocation.text = self.selectedLocation
        self.txtSelectedShelf.text = self.selectedShelf
        self.txtSelectedUser.text = self.selectedUser
        
        self.dfSelected.dateFormat = "dd/MM/yyyy"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case Storyboard.showSelectBranch:
            if let nvc = segue.destination as? UINavigationController {
                if let vc = nvc.viewControllers[0] as? SelectCriteriaTableViewController {
                    vc.preferredContentSize = CGSize(width: 400, height: 400)
                    vc.delegate = self
                    vc.selectedCriteria = .branch
                    
                    if let _ = self.selectedBranch {
                        vc.selectedElement = self.selectedBranch
                    } else {
                        vc.selectedElement = SELECT_ALL
                    }
                    vc.navTitle.title = "Select Branch"
                }
            }
        case Storyboard.showSelectDate:
            if let nvc = segue.destination as? UINavigationController {
                if let vc = nvc.viewControllers[0] as? SelectDateViewController {
                    vc.preferredContentSize = CGSize(width: 300, height: 300)
                    vc.delegate = self
                    if let _ = self.selectedDate {
                        vc.selectedDate = self.selectedDate
                    } else {
                        let def = UserDefaults.standard
                        vc.selectedDate = def.value(forKey: StockTakeInfoList.dateMax.rawValue) as? Date
                    }
                }
            }
        case Storyboard.showSelectLocation:
            if let nvc = segue.destination as? UINavigationController {
                if let vc = nvc.viewControllers[0] as? SelectCriteriaTableViewController {
                    vc.preferredContentSize = CGSize(width: 400, height: 400)
                    vc.delegate = self
                    vc.selectedCriteria = .location
                    
                    if let _ = self.selectedLocation {
                        vc.selectedElement = self.selectedLocation
                    } else {
                        vc.selectedElement = SELECT_ALL
                    }
                    vc.navTitle.title = "Select Location"
                }
            }
        case Storyboard.showSelectShelf:
            if let nvc = segue.destination as? UINavigationController {
                if let vc = nvc.viewControllers[0] as? SelectCriteriaTableViewController {
                    vc.preferredContentSize = CGSize(width: 400, height: 400)
                    vc.delegate = self
                    vc.selectedCriteria = .shelf
                    
                    if let _ = self.selectedShelf {
                        vc.selectedElement = self.selectedShelf
                    } else {
                        vc.selectedElement = SELECT_ALL
                    }
                    vc.navTitle.title = "Select Shelf"
                }
            }
        case Storyboard.showSelectUser:
            if let nvc = segue.destination as? UINavigationController {
                if let vc = nvc.viewControllers[0] as? SelectCriteriaTableViewController {
                    vc.preferredContentSize = CGSize(width: 400, height: 400)
                    vc.delegate = self
                    vc.selectedCriteria = .user
                    
                    if let _ = self.selectedUser {
                        vc.selectedElement = self.selectedUser
                    } else {
                        vc.selectedElement = SELECT_ALL
                    }
                    vc.navTitle.title = "Select User"
                }
            }
        case Storyboard.generateReport:
            if let vc = segue.destination as? StockTakeReportViewController {
                // check stock take record
                let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
                let context = appDelegate.persistentContainer.viewContext
                do {
                    let countStockTake = try context.count(for:StockTakeRecord.fetchRequest())
                    self.hasStockTakeRecord = (countStockTake != 0) ? true : false
                } catch let error as NSError {
                    print("Error: \(error.localizedDescription)")
                }
                
                if self.hasStockTakeRecord == false {
                    let action = UIAlertAction(title: "Close", style: .default, handler: nil)
                    let alertController = UIAlertController(title: "Error", message: "No record found." , preferredStyle: .alert)
                    alertController.addAction(action)
                    
                    present(alertController, animated: true, completion: nil)
                    
                } else {
                    vc.selectedBranch = self.selectedBranch
                    vc.selectedDateString = self.selectedDateString
                    vc.selectedLocation = self.selectedLocation
                    vc.selectedShelf = self.selectedShelf
                    vc.selectedUser = self.selectedUser
                    
                    vc.dfSelected = self.dfSelected
                }
            }
        default:
            break
        }
    }
    
    @IBAction func btnAllClicked(_ sender: Any) {
        self.viewDidLoad()
    }
}

// MARK: SelectDateViewControllerDelegate
extension GenerateReportViewController: SelectDateViewControllerDelegate {
    func didSelectDate(_ sender: UIDatePicker, selectedDate: Date) {
        // Set date
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        self.selectedDate = selectedDate
        self.btnSelectedDate.setTitle("\(df.string(from: selectedDate))", for: .normal)
        self.selectedDateString = df.string(from: selectedDate)
    }
}
