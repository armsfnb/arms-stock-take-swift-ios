//
//  ViewControllerExt.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 10/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension UIViewController {
    
    public func commit() {
        if let appDelegate = self.appDelegate() {
            appDelegate.saveContext()
        }
    }
    
    internal func appDelegate() -> AppDelegate! {
        var appDelegate: AppDelegate?
        if let delegate = UIApplication.shared.delegate {
            appDelegate = delegate as? AppDelegate
        }
        return appDelegate
    }
    
    internal func appContext() -> NSManagedObjectContext {
        let context = self.appDelegate().persistentContainer.viewContext
        return context
    }
}
