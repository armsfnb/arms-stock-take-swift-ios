//
//  StockTakeOperationByBatchViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 29/03/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//

import Foundation
import UIKit

//class StockTakeOperationByBatchViewController: UITableViewController {
//
//    @IBOutlet weak var btnSelectDateStockTake: UIButton!
//    @IBOutlet weak var switchQtyOne: UISwitch!
//    @IBOutlet weak var lblStockTakeError: UILabel!
//
//    var selectedDate: Date?
//    var isQtyOne: Bool?
//
//    @IBAction func btnConfirmStockTake(_ sender: Any) {
//        self.isQtyOne = self.switchQtyOne.isOn
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.lblStockTakeError.text = ""
//        // Do any additional setup after loading the view, typically from a nib.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let nvc = segue.destination as? UINavigationController {
//            if let vc = nvc.viewControllers[0] as? SelectDateViewController {
//                vc.preferredContentSize = CGSize(width: 300, height: 300)
//                vc.delegate = self
//
//                if let _ = self.selectedDate {
//                    vc.selectedDate = self.selectedDate
//                } else {
//                    vc.selectedDate = Date()
//                }
//            }
//        } else if let vc = segue.destination as? StockTakeOperationViewController {
//            if let _ = self.isQtyOne {
//                vc.isQtyOne = self.isQtyOne
//            } else {
//                vc.isQtyOne = false
//            }
//        }
//    }
//}
//
//class StockTakeOperationViewController: UITableViewController {
//
//    @IBOutlet weak var lblQtyOne: UILabel!
//    @IBOutlet weak var lblStockTakeItemError: UILabel!
//    @IBOutlet weak var txtStockQuantity: UITextField!
//
//    var isQtyOne: Bool?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.lblStockTakeItemError.text = ""
//
//        switch self.isQtyOne! {
//        case true:
//            self.lblQtyOne.isHidden = false
//            self.txtStockQuantity.isEnabled = false
//            self.txtStockQuantity.text = "1"
//        case false:
//            self.lblQtyOne.isHidden = true
//            self.txtStockQuantity.isEnabled = true
//            self.txtStockQuantity.text = ""
//        }
//        // Do any additional setup after loading the view, typically from a nib.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//}
//
//
//extension StockTakeOperationByBatchViewController: SelectDateViewControllerDelegate {
//    func didSelectDate(_ sender: UIDatePicker, selectedDate: Date) {
//        let df = DateFormatter()
//        df.dateFormat = "dd/MM/yyyy"
//        self.selectedDate = selectedDate
//        self.btnSelectDateStockTake.setTitle("\(df.string(from: self.selectedDate!))", for: .normal)
//    }
//}
//
