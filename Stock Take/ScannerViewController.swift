//
//  ScannerViewController.swift
//  Stock Take
//
//  Created by Yong Meng Lee on 04/04/2018.
//  Copyright © 2018 Yong Meng Lee. All rights reserved.
//
// Source: https://www.hackingwithswift.com/example-code/media/how-to-scan-a-qr-code

import AVFoundation
import UIKit

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var viewFocus: UIImageView!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
//    var delegate: StockTakeOperationViewController?
    
    var delegate: UITableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            // supports all barcode types
            metadataOutput.metadataObjectTypes = [.qr, .aztec, .code128, .code39, .code39Mod43, .code93, .dataMatrix, .ean13, .ean8, .face, .interleaved2of5, .itf14, .pdf417, .upce]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
        
        view.bringSubview(toFront: self.topBar)
        view.bringSubview(toFront: self.bottomBar)
        view.bringSubview(toFront: self.viewFocus)
    }
    
    @IBAction func unwindToHomeScreen(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func failed() {
        let alertController = UIAlertController(title: "Scanning Not Supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertController, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied:
            let alertController = UIAlertController(title: "Camera Access Denied", message: "Please allow camera access in your device settings and restart this app." , preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "Close", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
            break
//        case .notDetermined:
//            break
//        case .authorized:
//            break
//        case .restricted:
//            break
        default:
            break
        }
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
            
            if let vc = self.delegate as? StockTakeOperationViewController {
                if vc.isQtyOne == false {
                    vc.txtStockQuantity.becomeFirstResponder()
                }
            }
            
            // Activating Stock Qty textfield by default (Qty is not fixed to 1)
//            if let _ = self.delegate {
//                if self.delegate?.isQtyOne == false {
//                    self.delegate?.txtStockQuantity.becomeFirstResponder()
//                }
//            }
        }
        
        self.dismiss(animated: true)
    }
    
    func found(code: String) {
        print(code)
        
        if let vc = self.delegate as? StockTakeOperationViewController {
            vc.txtStockID.text = code
            
            if vc.isQtyOne == true {
                // auto-save feature
                vc.saveStockTakeData()
                vc.txtStockID.text = ""
            }
        } else if let vc = self.delegate as? PriceCheckViewController {
            vc.txtScanProduct.text = code
            vc.code = vc.txtScanProduct.text
//            vc.searchItem()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
